package com.example.mnemonicassignment.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(uniqueConstraints = {})
public class Transaction {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private long registeredTime;
    private long executedTime;
    private boolean success;
    private double cashAmount;

    @ManyToOne(fetch = FetchType.LAZY)
    private Account sourceAccount;

    @ManyToOne(fetch = FetchType.LAZY)
    private Account destinationAccount;

    public Transaction(double cashAmount, Account sourceAccount, Account destinationAccount) {
        this.cashAmount = cashAmount;
        this.sourceAccount = sourceAccount;
        this.destinationAccount = destinationAccount;
    }
}
