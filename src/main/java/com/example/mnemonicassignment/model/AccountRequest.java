package com.example.mnemonicassignment.model;

public class AccountRequest {
    private String name;
    private double availableCash;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getAvailableCash() {
        return availableCash;
    }

    public void setAvailableCash(double availableCash) {
        this.availableCash = availableCash;
    }
}
