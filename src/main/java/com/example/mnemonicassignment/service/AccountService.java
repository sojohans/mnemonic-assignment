package com.example.mnemonicassignment.service;

import com.example.mnemonicassignment.model.Account;
import com.example.mnemonicassignment.model.AccountRequest;
import com.example.mnemonicassignment.repository.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * Service class responsible for managing account-related operations.
 */
@Service
public class AccountService {

    private final AccountRepository accountRepository;

    /**
     * Constructs a new instance of the AccountService.
     *
     * @param accountRepository The repository for managing accounts.
     */
    @Autowired
    public AccountService(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    /**
     * Creates a new account based on the provided account request.
     *
     * @param accountRequest The request containing account information.
     * @return A ResponseEntity with a message indicating the result of the account creation.
     */
    public ResponseEntity<String> createAccount(AccountRequest accountRequest) {
        // Check if an account with the same name already exists
        Optional<Account> existingAccount = accountRepository.findAccountByName(accountRequest.getName());
        if (existingAccount.isPresent()) {
            String response = "Account with given name already exists.";
            return ResponseEntity.status(HttpStatus.CONFLICT).body(response);
        }

        // Create a new account and save it to the repository
        Account account = new Account(accountRequest.getName(), accountRequest.getAvailableCash());
        accountRepository.save(account);
        return ResponseEntity.ok("Account created.");
    }


    /**
     * Edits an existing account based on the provided account ID and request.
     *
     * @param accountId      The unique identifier of the account to edit.
     * @param accountRequest The request containing updated account information.
     * @return A ResponseEntity with a message indicating the result of the account edit.
     */
    public ResponseEntity<String> editAccount(long accountId, AccountRequest accountRequest) {
        // Check if the account with the specified ID exists
        Optional<Account> optionalAccount = accountRepository.findById(accountId);
        if (optionalAccount.isEmpty()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Account not found");
        }

        // Update the account details and save the changes
        Account account = optionalAccount.get();
        account.setName(accountRequest.getName());
        account.setAvailableCash(accountRequest.getAvailableCash());
        accountRepository.save(account);
        return ResponseEntity.ok("Account edited.");
    }

    /**
     * Deletes an account based on the provided account ID.
     *
     * @param accountId The unique identifier of the account to delete.
     * @return A ResponseEntity with a message indicating the result of the account deletion.
     */
    public ResponseEntity<String> deleteAccount(long accountId) {
        // Check if the account with the specified ID exists
        if (!accountRepository.existsById(accountId)) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Account not found");
        }
        // Delete the account
        accountRepository.deleteById(accountId);
        return ResponseEntity.ok("Account deleted.");
    }


    /**
     * Retrieves a list of all accounts.
     *
     * @return A ResponseEntity containing a list of accounts and an HTTP OK status.
     */
    public ResponseEntity<List<Account>> getAllAccounts() {
        return ResponseEntity.status(HttpStatus.OK).body(accountRepository.findAll());
    }

    /**
     * Retrieves an account by its unique identifier (ID).
     *
     * @param accountId The unique identifier of the account to retrieve.
     * @return A ResponseEntity containing the account and an HTTP OK status if found,
     *         or an HTTP NOT FOUND status if the account does not exist.
     */
    public ResponseEntity<Account> getAccountById(long accountId) {
        // Check if the account with the specified ID exists
        Optional<Account> existingAccount = accountRepository.findById(accountId);
        if (existingAccount.isEmpty()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
        // Return the account if found
        Account account = existingAccount.get();
        return ResponseEntity.status(HttpStatus.OK).body(account);
    }
}
