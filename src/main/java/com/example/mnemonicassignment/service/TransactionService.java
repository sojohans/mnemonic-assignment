package com.example.mnemonicassignment.service;

import com.example.mnemonicassignment.model.Account;
import com.example.mnemonicassignment.model.Transaction;
import com.example.mnemonicassignment.model.TransactionRequest;
import com.example.mnemonicassignment.repository.AccountRepository;
import com.example.mnemonicassignment.repository.TransactionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import java.util.Optional;

/**
 * Service class responsible for handling transaction-related operations.
 */
@Service
public class TransactionService {

    private final TransactionRepository transactionRepository;
    private final AccountRepository accountRepository;


    /**
     * Constructs a new instance of the TransactionService.
     *
     * @param transactionRepository The repository for managing transactions.
     * @param accountRepository     The repository for managing accounts.
     */
    @Autowired
    public TransactionService(TransactionRepository transactionRepository, AccountRepository accountRepository) {
        this.transactionRepository = transactionRepository;
        this.accountRepository = accountRepository;
    }


    /**
     * Creates a new transaction based on the provided transaction request.
     *
     * @param transactionRequest  The request containing details of the transaction.
     * @param currentTimeMillis  The current timestamp for the transaction.
     * @return A ResponseEntity with a message indicating the result of the transaction creation.
     */
    public ResponseEntity<String> createTransaction(TransactionRequest transactionRequest, long currentTimeMillis) {

        // Check if the source account exists
        Optional<Account> optionalSourceAccount = accountRepository.findById(transactionRequest.getSourceAccountId());
        if (optionalSourceAccount.isEmpty()) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Invalid source account");
        }

        // Check if the destination account exists
        Optional<Account> optionalDestinationAccount =
                accountRepository.findById(transactionRequest.getDestinationAccountId());
        if (optionalDestinationAccount.isEmpty()) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Invalid destination account");
        }

        // Get the source and destination accounts
        Account sourceAccount = optionalSourceAccount.get();
        Account destinationAccount = optionalDestinationAccount.get();

        // Check if the source and destination accounts are the same
        if (sourceAccount.equals(destinationAccount)) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body("Cannot perform transaction between the same account");
        }

        // Check if there is enough available cash in the source account
        if (transactionRequest.getCashAmount() > sourceAccount.getAvailableCash()){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Invalid cash amount");
        }

        // Create a new transaction
        Transaction transaction =  new Transaction();
        transaction.setCashAmount(transactionRequest.getCashAmount());
        transaction.setSourceAccount(sourceAccount);
        transaction.setDestinationAccount(destinationAccount);
        transaction.setRegisteredTime(currentTimeMillis);

        // Update the available cash in the source and destination accounts
        sourceAccount.setAvailableCash(sourceAccount.getAvailableCash() - transaction.getCashAmount());
        destinationAccount.setAvailableCash(destinationAccount.getAvailableCash() + transaction.getCashAmount());

        transaction.setExecutedTime(System.currentTimeMillis());
        transaction.setSuccess(true);

        // Save the transaction to the repository
        transactionRepository.save(transaction);
        return ResponseEntity.status(HttpStatus.OK).body("Transaction successful: \n" + transaction);
    }
}
