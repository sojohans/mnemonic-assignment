package com.example.mnemonicassignment.service;

import com.example.mnemonicassignment.model.Account;
import com.example.mnemonicassignment.repository.AccountRepository;
import jakarta.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


/**
 * The DataSeeder class is responsible for initializing and seeding the database with dummy account data
 * upon application startup.
 */
@Service
public class DataSeeder {

    private final AccountRepository accountRepository;


    /**
     * Constructs a new DataSeeder instance with the specified AccountRepository.
     *
     * @param accountRepository The repository for managing Account entities.
     */
    @Autowired
    public DataSeeder(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }


    /**
     * The seedData method is annotated with @PostConstruct, which means it is executed automatically
     * during application startup. This method populates the database with dummy account data.
     */
    @PostConstruct
    public void seedData() {
        // Create and save dummy accounts
        Account account1 = new Account("Account 1", 1000.0);
        Account account2 = new Account("Account 2", 2000.0);
        Account account3 = new Account("Account 3", 3000.0);

        // Save the dummy accounts to the database using the AccountRepository.
        accountRepository.save(account1);
        accountRepository.save(account2);
        accountRepository.save(account3);
    }
}
