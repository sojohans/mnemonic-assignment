package com.example.mnemonicassignment.controller;

import com.example.mnemonicassignment.model.Account;
import com.example.mnemonicassignment.model.AccountRequest;
import com.example.mnemonicassignment.service.AccountService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * Controller class responsible for managing account-related operations.
 *
 * This controller handles operations related to creating, retrieving, editing, and deleting
 * accounts, and it interacts with the AccountService to perform these operations.
 */
@RestController
@CrossOrigin("*")
@RequestMapping("/account")
public class AccountController {

    private final AccountService accountService;

    /**
     * Constructs a new instance of the AccountController.
     *
     * @param accountService The service responsible for account-related operations.
     */
    @Autowired
    public AccountController(AccountService accountService) {
        this.accountService = accountService;
    }

    /**
     * Creates a new account based on the provided account request.
     *
     * @param accountRequest The request containing account information.
     * @return A ResponseEntity with a message indicating the result of the account creation,
     *         along with an HTTP status code (200 for success, 409 for a conflict).
     */
    @PostMapping
    @Operation(description = "Endpoint for creating a new account.", responses = { @ApiResponse( description = "Success",
            responseCode = "200"), @ApiResponse( description = "Conflict", responseCode = "409")})
    public ResponseEntity<String> createAccount(@RequestBody AccountRequest accountRequest) {
        return accountService.createAccount(accountRequest);
    }

    /**
     * Retrieves a list of all accounts.
     *
     * @return A ResponseEntity with the list of accounts and an HTTP status code (200 for success).
     */
    @GetMapping
    @Operation(description = "Endpoint for fetching all accounts.", responses = { @ApiResponse( description = "Success",
            responseCode = "200")})
    public ResponseEntity<List<Account>> getAllAccounts() {
        return accountService.getAllAccounts();
    }

    /**
     * Edits an existing account with the specified account ID based on the provided account request.
     *
     * @param accountId       The unique identifier of the account to be edited.
     * @param accountRequest  The request containing updated account information.
     * @return A ResponseEntity with a message indicating the result of the account editing,
     *         along with an HTTP status code (200 for success, 404 for not found).
     */
    @PutMapping("/{accountId}")
    @Operation(description = "Endpoint for editing an account.", responses = { @ApiResponse( description = "Success",
            responseCode = "200"), @ApiResponse( description = "Not Found", responseCode = "404")})
    public ResponseEntity<String> editAccount(
            @PathVariable long accountId,
            @RequestBody AccountRequest accountRequest
    ) {
        return accountService.editAccount(accountId, accountRequest);
    }

    /**
     * Deletes an existing account with the specified account ID.
     *
     * @param accountId  The unique identifier of the account to be deleted.
     * @return A ResponseEntity with a message indicating the result of the account deletion,
     *         along with an HTTP status code (200 for success, 404 for not found).
     */
    @DeleteMapping("/{accountId}")
    @Operation(description = "Endpoint for deleting an account.", responses = { @ApiResponse( description = "Success",
            responseCode = "200"), @ApiResponse( description = "Not Found", responseCode = "404")}
    )
    public ResponseEntity<String> deleteAccount(@PathVariable long accountId) {
        return accountService.deleteAccount(accountId);
    }

    /**
     * Retrieves an account with the specified account ID.
     *
     * @param accountId  The unique identifier of the account to be retrieved.
     * @return A ResponseEntity with the account information and an HTTP status code (200 for success).
     */
    @GetMapping("/{accountId}")
    @Operation(description = "Endpoint for fetching an account.", responses = { @ApiResponse(description = "Success",
            responseCode = "200")})
    public ResponseEntity<Account> getAccountById(@PathVariable long accountId) {
        return accountService.getAccountById(accountId);
    }

}
