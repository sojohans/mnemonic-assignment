package com.example.mnemonicassignment.controller;

import com.example.mnemonicassignment.model.TransactionRequest;
import com.example.mnemonicassignment.service.TransactionService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * Controller class responsible for managing transaction-related operations.
 *
 * This controller handles the creation of new transactions and interacts with the
 * TransactionService to process transaction requests.
 */
@RestController
@CrossOrigin("*")
@RequestMapping("/transaction")
public class TransactionController {

    private final TransactionService transactionService;

    /**
     * Constructs a new instance of the TransactionController.
     *
     * @param transactionService The service responsible for transaction-related operations.
     */
    @Autowired
    public TransactionController(TransactionService transactionService) {
        this.transactionService = transactionService;
    }

    /**
     * Creates a new transaction based on the provided transaction request.
     *
     * @param transactionRequest The request containing transaction information.
     * @return A ResponseEntity with a message indicating the result of the transaction creation,
     *         along with an HTTP status code (200 for success, 400 for a bad request).
     */
    @PostMapping
    @Operation(description = "Endpoint for creating a new transaction.", responses = { @ApiResponse( description
            = "Success", responseCode = "200"), @ApiResponse( description = "Bad Request", responseCode = "400")})
    public ResponseEntity<String> createTransaction(@RequestBody TransactionRequest transactionRequest) {
        return transactionService.createTransaction(transactionRequest, System.currentTimeMillis());
    }

}
