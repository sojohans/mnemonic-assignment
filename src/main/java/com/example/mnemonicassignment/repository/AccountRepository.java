package com.example.mnemonicassignment.repository;

import com.example.mnemonicassignment.model.Account;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface AccountRepository extends JpaRepository<Account, Long> {

    Optional<Account> findAccountByName(String name);
}
