package com.example.mnemonicassignment;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@OpenAPIDefinition( info = @Info( title = "Mnemonic REST API Assignment", version = "1.0.0",
        description = "API documentation"))
public class MnemonicAssignmentApplication {
    public static void main(String[] args) {
        SpringApplication.run(MnemonicAssignmentApplication.class, args);
    }
}
